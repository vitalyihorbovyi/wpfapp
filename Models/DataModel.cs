﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models.Serialization;

namespace WpfApp.Models
{
    [Serializable]
    public class DataModel
    {
        public IEnumerable<Customer> Customers { get; set; }

        public IEnumerable<Parcel> Parcels { get; set; }

        public IEnumerable<Delivery> Deliveries { get; set; }

        public DataModel()
        {
            Customers = new List<Customer>();
            Parcels = new List<Parcel>();
            Deliveries = new List<Delivery>();
        }

        public static string DataPath = "data.dat";

        public static DataModel Load()
        {
            if (File.Exists(DataPath))
            {
                return DataSerializer.DeserializeItem(DataPath);
            }
            return new DataModel();
        }

        public void Save()
        {
            DataSerializer.SerializeData(DataPath, this);
        }
    }
}
