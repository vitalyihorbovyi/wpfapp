﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public enum DeliveryStatus
    {
        Delivered,
        Proccessing,
        Received,
        Cancelled
    }
}
