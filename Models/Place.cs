﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WpfApp.Models
{
    [Serializable]
    public class Place
    {
        public Country Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int PlaceNumber { get; set; }

        public override string ToString()
        {
            return String.Format("{0}, {1}, {2} {3}", Country, City, Street, PlaceNumber);
        }
    }
}
