﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    [Serializable]
    public class Parcel
    {
        public int ParcelNumber { get; set; }

        public double Weight { get; set; }

        public string Name { get; set; }

    }
}
