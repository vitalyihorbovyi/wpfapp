﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public enum Country
    {
        Brazil,
        Ukraine,
        England,
        Spain,
        France,
        Germany,
        Belgium,
        Portugal,
        Argentina,
        Poland,
        USA,
        Sweden,
        Columbia,
        Italy,
        Croatia,
        Senegal,
        Egypt,
        Uruguay,
        Netherlands,
        Denmark,
        Serbia,
        Scotland,
        Greece,
        Chile,
        SouthKorea,
        Slovenia,
        Gabon,
        Wales
    }
}
