﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    [Serializable]
    public class Customer
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MobileNumber { get; set; }
        public Place Residence { get; set; }
        public Parcel Parcel { get; set; }
    }
}
