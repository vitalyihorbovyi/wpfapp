﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    [Serializable]
    public class Delivery
    {
        public DeliveryStatus Status { get; set; }
        public DateTime RegistrationTime { get; set; }
        public int DeliveryDaysNumber { get; set; }
        public decimal Price { get; set; }
        public Parcel Parcel { get; set; }
        public Place Destination { get; set; }
    }
}
