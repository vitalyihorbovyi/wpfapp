﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using WpfApp.Models;

namespace WpfApp.Converters
{
    public class StatusImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = ((DeliveryStatus)value).ToString();
            var uri = new Uri(string.Format(@"C:\University\3 course\5 sem\КПЗ\Lab2\WpfApp\WpfApp\Images\Statuses\{0}.png", status));
            return new BitmapImage(uri);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
