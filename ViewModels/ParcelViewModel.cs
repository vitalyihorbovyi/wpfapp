﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.ViewModels
{
    public class ParcelViewModel : ViewModelBase
    {
        private int _ParcelNumber { get; set; }
        public int ParcelNumber
        {
            get
            {
                return _ParcelNumber;
            }
            set
            {
                _ParcelNumber = value;
                OnPropertyChanged(nameof(ParcelNumber));
            }
        }


        private double _Weight { get; set; }
        public double Weight
        {
            get
            {
                return _Weight;
            }
            set
            {
                _Weight = value;
                OnPropertyChanged(nameof(Weight));
            }
        }

        private string _Name { get; set; }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
    }
}
