﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;

namespace WpfApp.ViewModels
{
    public class CustomerViewModel : ViewModelBase
    {
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private string _Surname;
        public string Surname
        {
            get
            {
                return _Surname;
            }
            set
            {
                _Surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }

        private string _MobileNumber;
        public string MobileNumber
        {
            get
            {
                return _MobileNumber;
            }
            set
            {
                _MobileNumber = value;
                OnPropertyChanged(nameof(MobileNumber));
            }
        }

        private Place _Residence;
        public Place Residence
        {
            get
            {
                return _Residence;
            }
            set
            {
                _Residence = value;
                OnPropertyChanged(nameof(Residence));
            }
        }


        private Parcel _Parcel;
        public Parcel Parcel
        {
            get
            {
                return _Parcel;
            }
            set
            {
                _Parcel = value;
                OnPropertyChanged(nameof(Parcel));
            }
        }
    }
}
