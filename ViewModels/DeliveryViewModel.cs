﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;

namespace WpfApp.ViewModels
{
    public class DeliveryViewModel : ViewModelBase
    {
        private DeliveryStatus _Status;
        public DeliveryStatus Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        private DateTime _RegistrationTime;
        public DateTime RegistrationTime
        {
            get
            {
                return _RegistrationTime;
            }
            set
            {
                _RegistrationTime = value;
                OnPropertyChanged(nameof(RegistrationTime));
            }
        }

        private int _DeliveryDaysNumber;
        public int DeliveryDaysNumber
        {
            get
            {
                return _DeliveryDaysNumber;
            }
            set
            {
                _DeliveryDaysNumber = value;
                OnPropertyChanged(nameof(DeliveryDaysNumber));
            }
        }

        private decimal _Price;
        public decimal Price
        {
            get
            {
                return _Price;
            }
            set
            {
                _Price = value;
                OnPropertyChanged(nameof(Price));
            }
        }


        private Parcel _Parcel;
        public Parcel Parcel
        {
            get
            {
                return _Parcel;
            }
            set
            {
                _Parcel = value;
                OnPropertyChanged(nameof(Parcel));
            }
        }

        private Place _Destination;
        public Place Destination
        {
            get
            {
                return _Destination;
            }
            set
            {
                _Destination = value;
                OnPropertyChanged(nameof(Destination));
            }
        }
    }
}
