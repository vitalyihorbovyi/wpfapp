﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Models;

namespace WpfApp.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        DataViewModel()
        {
            RemoveDeliveryCommand = new Command(RemoveDelivery);
        }
        public ICommand RemoveDeliveryCommand { get; set; }

        private void RemoveDelivery(object parameter)
        {
            int index = Deliveries.IndexOf(parameter as DeliveryViewModel);
            if (index >= 0 && index < Deliveries.Count)
            {
                Deliveries.RemoveAt(index);
            }
        }

        private ObservableCollection<DeliveryViewModel> _Deliveries;
        public ObservableCollection<DeliveryViewModel> Deliveries
        {
            get
            {
                return _Deliveries;
            }
            set
            {
                _Deliveries = value;
                OnPropertyChanged(nameof(Deliveries));
            }
        }

        private ObservableCollection<CustomerViewModel> _Customers;

        public ObservableCollection<CustomerViewModel> Customers
        {
            get
            {
                return _Customers;
            }
            set
            {
                _Customers = value;
                OnPropertyChanged(nameof(Customers));
            }
        }

        private ObservableCollection<ParcelViewModel> _Parcels;

        public ObservableCollection<ParcelViewModel> Parcels
        {
            get
            {
                return _Parcels;
            }
            set
            {
                _Parcels = value;
                OnPropertyChanged(nameof(Parcels));
            }
        }
    }
}
