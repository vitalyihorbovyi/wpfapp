﻿using AutoMapper;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;
using WpfApp.ViewModels;

namespace WpfApp.Mapping
{
    public class Map
    {
        public MapperConfiguration config { get; set; }
        public Map()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DataModel, DataViewModel>();
                cfg.CreateMap<DataViewModel, DataModel>();
                cfg.CreateMap<Customer, CustomerViewModel>();
                cfg.CreateMap<CustomerViewModel, Customer>();
                cfg.CreateMap<Delivery, DeliveryViewModel>();
                cfg.CreateMap<DeliveryViewModel, Delivery>();
                cfg.CreateMap<Parcel, ParcelViewModel>();
                cfg.CreateMap<ParcelViewModel, Parcel>();
            });
        }
    }

    public class MappingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(context => new Map().config.CreateMapper());
        }
    }
}
