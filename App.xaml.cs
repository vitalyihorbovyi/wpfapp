﻿using AutoMapper;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfApp.Mapping;
using WpfApp.Models;
using WpfApp.ViewModels;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
   
    public partial class App : Application
    {
        private DataModel _model;
        private DataViewModel _viewModel;
        private IMapper mapper;

        public App()
        {
            _model = DataModel.Load();
            var kernel = new StandardKernel(new MappingModule());
            mapper = kernel.Get<IMapper>();
            _viewModel = mapper.Map<DataModel, DataViewModel>(_model);
            var window = new MainWindow() { DataContext = _viewModel };
            window.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                _model = mapper.Map<DataViewModel, DataModel>(_viewModel);
                _model.Save();
            }
            catch (Exception)
            {
                base.OnExit(e);
            }
        }
    }
}
